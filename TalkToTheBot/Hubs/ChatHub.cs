﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Syn.Bot;
using System.IO;
using Syn.Bot.Events;
using System.Diagnostics;
using System.Web;

namespace TalkToTheBot
{
    public class ChatHub : Hub
    {
        public static SynBot bot = new SynBot();
        public static List<BotUser> botUsers = new List<BotUser>();
        public static string simlPackage = File.ReadAllText(HttpContext.Current.Server.MapPath("\\SimlBrain\\bot.simlpk"));
        
        public override Task OnConnected()
        {
            var botUser = new BotUser(bot, Context.ConnectionId);
            botUsers.Add(botUser);
            bot.Learning += Bot_Learning;
            bot.Memorizing += Bot_Memorizing;
            bot.Configuration.StoreVocabulary = true;
            bot.Configuration.StoreExamples = true;

            //Loading brain
            bot.PackageManager.LoadFromString(simlPackage);
            try
            {
                var fileLearn = File.ReadAllText(Path.Combine(HttpContext.Current.Server.MapPath("~"), "SimlBrain\\Learned.siml"));
                bot.AddSiml(fileLearn);
                Debug.WriteLine("Loaded learning file...");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Project Amy has not learned anything new yet...");
            }
            //try
            //{
            //    var memPackage = File.ReadAllText("SimlBrain\\" + botUser.ID + "\\memory.simlpk");
            //    bot.PackageManager.LoadFromString(memPackage);
            //    Debug.WriteLine("Loaded personal user Memory file...");
            //}
            //catch (Exception ex)
            //{
            //    Debug.WriteLine("No memory file for user " + botUser.ID);
            //}

            var interactions = bot.Stats.Interactions;
            var idleTime = bot.Stats.IdleTime;
            var loadTime = bot.Stats.LoadTime;
            var mappingTime = bot.Stats.MappingTime;
            var modelCount = bot.Stats.ModelCount;

            return base.OnConnected();
        }

        public override Task OnDisconnected(bool stopCalled)
        {
            return base.OnDisconnected(stopCalled);
        }

        public void SendToBot(string message)
        {
            var botUser = botUsers.First(m => m.ID == Context.ConnectionId);
            var chatReq = new ChatRequest(message, botUser);
            var response = bot.Chat(chatReq);
            if (response.Success)
            {
                Debug.WriteLine("Bot: " + response.BotMessage);
                Clients.Caller.messageReceived(response.BotMessage);
            }
        }

        static void Bot_Learning(object sender, LearningEventArgs e)
        {
            try
            {
                Debug.WriteLine("Writing to learn file...");
                var filePath = Path.Combine(HttpContext.Current.Server.MapPath("~"), "\\SimlBrain\\Learned.siml");
                e.Document.Save(filePath);
                Debug.WriteLine("Saved learn file!");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Write Failed: " + ex.Message);
            }
        }
        static void Bot_Memorizing(object sender, MemorizingEventArgs e)
        {
            Random random = new Random();
            Guid memoryId = Guid.NewGuid();
            Debug.WriteLine("Writing to memory file for user: " + e.User.ID);
            try
            {
                if (Directory.Exists(HttpContext.Current.Server.MapPath("\\SimlBrain\\" + e.User.ID) ))
                {
                    var filePath = Path.Combine(HttpContext.Current.Server.MapPath("\\SimlBrain"), e.User.ID, "Memorized-" + memoryId.ToString() + ".siml");
                    e.Document.Save(filePath);
                }
                else
                {
                    Directory.CreateDirectory(HttpContext.Current.Server.MapPath("\\SimlBrain\\" + e.User.ID));
                    var filePath = Path.Combine(HttpContext.Current.Server.MapPath("\\SimlBrain"), e.User.ID, "Memorized" + memoryId.ToString() + ".siml");
                    e.Document.Save(filePath);
                }
                Debug.WriteLine("Write success!");
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Write failed: " + ex.Message);
            }
        }
    }
}
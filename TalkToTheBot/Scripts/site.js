﻿var app = angular.module("talkToTheBotApp", []);

var controller = app.controller("botController", function ($scope) {
    var chat = $.connection.chatHub
    $.connection.hub.logging = false;
    $scope.messages = [];

    $.connection.hub.start().done(function () {
        $('#messageInput').keypress(function (e) {
            if (e.which == 13) {
                $scope.sendMessage();
            }
        });

        $scope.sendMessage = function () {
            if ($('#messageInput').val() != '') {
                console.log("sending message to bot...");
                chat.server.sendToBot($('#messageInput').val());
                $scope.messages.unshift({ 'message': $("#messageInput").val(), 'user': 'You' });
                $('#messageInput').val('');
                $('#messageInput').focus();
            }
        }    
    });

    chat.client.messageReceived = function (message) {
        console.log("message from bot received: " + message);
        $scope.messages.unshift({ 'message': message, 'user': 'Bot' });
        $scope.$apply();
    };

    //$(window).bind('scroll', function () {
    //    if ($(window).scrollTop() > 570) {
    //        $('.ui.sticky').sticky({
    //            context: "#context"
    //        });
    //    }
    //});  
});

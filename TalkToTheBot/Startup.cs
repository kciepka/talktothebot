﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TalkToTheBot.Startup))]
namespace TalkToTheBot
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
